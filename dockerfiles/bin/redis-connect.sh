#!/usr/bin/env bash
redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} -n ${REDIS_DB:-1}
