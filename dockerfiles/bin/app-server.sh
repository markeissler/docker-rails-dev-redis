#!/usr/bin/env bash
#
# Start a server, restart if it crashes, but still support exiting if it does
# not crash.
#
source /etc/environment

# location of main pkg
app_home="${BUILD_HOME}"
app_main="${BUILD_HOME}/source"
server_cmd="${BUNDLE_BIN}/puma -C config/puma.rb"

cd "${app_main}"
${server_cmd}
