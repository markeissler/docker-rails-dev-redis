#!/usr/bin/env bash

DB_NAME="${APPNAME}"

echo "Dropping database..."
PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} \
    -c "DROP DATABASE IF EXISTS \"${DB_NAME}\""

echo "Creating database..."
PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} \
    -c "CREATE DATABASE \"${DB_NAME}\" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';"

if [[ -f "${SOURCE_DIR}/dockerfiles/data_seed/data/postgres.dump" ]]
then
    echo "Loading data..."
    PGPASSWORD=${POSTGRES_PASSWORD} pg_restore -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} \
        --no-acl --no-owner --verbose \
        --dbname "\"${DB_NAME}\"" "${SOURCE_DIR}/dockerfiles/data_seed/data/postgres.dump"
fi

# add one or more table names within the (), separated by space or newlines
tables=()

if [[ "${#tables[@]}" -gt 0 ]]
then
    echo "Refreshing materialized views..."

    for t in "${tables[@]}"; do
        PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} \
            --dbname "\"${DB_NAME}\"" -c "REFRESH MATERIALIZED VIEW ${t}"
        echo "-> ${t}"
    done
fi

echo "Database updated"
