#!/usr/bin/env bash
#
# docker-entrypoint.sh for app
#

configActiveStorage() {
    if [[ ! -f "${SOURCE_DIR}/config/storage.yml" ]]; then
        printf "%s\n-> " "-> Configuring Active Storage..."

        # run rake task to add new migration
        # rake active_storage:install

cat <<- 'EOF' > "${SOURCE_DIR}/config/storage.yml"
test:
  service: Disk
  root: <%= Rails.root.join("tmp/storage") %>

local:
  service: Disk
  root: <%= Rails.root.join("storage") %>

amazon:
  service: Disk
  root: <%= Rails.root.join("storage_amazon") %>
EOF
        # set perms
        chmod 0644 "${SOURCE_DIR}/config/storage.yml"
        chown "${BUILD_USER}":"${BUILD_USER}" "${SOURCE_DIR}/config/storage.yml"

        # add local amazon storage space
        mkdir -p "${SOURCE_DIR}/storage_amazon"
        chmod 0755 "${SOURCE_DIR}/storage_amazon"
        chown "${BUILD_USER}":"${BUILD_USER}" "${SOURCE_DIR}/storage_amazon"

        printf "%s\n" "-> Active Storage configured"
    fi
}

configMySQL() {
    if [[ ! -f "${SOURCE_DIR}/config/database.yml" ]]; then
        printf "%s\n-> " "-> Configuring MySQL..."

cat <<- 'EOF' > "${SOURCE_DIR}/config/database.yml"
default: &default
  adapter: mysql2
  encoding: utf8
  pool: 5
  username: <%= ENV['MYSQL_USER'] %>
  password: <%= ENV['MYSQL_PASSWORD'] %>
  host: <%= ENV['MYSQL_HOST'] %>
  port: <%= ENV['MYSQL_PORT'] %>

development:
  <<: *default
  database: <%= ENV['APPNAME'] %>_development

test:
  <<: *default
  database: <%= ENV['APPNAME'] %>_test
EOF
        # set perms
        chmod 0644 "${SOURCE_DIR}/config/database.yml"
        chown "${BUILD_USER}":"${BUILD_USER}" "${SOURCE_DIR}/config/database.yml"

        printf "%s\n" "-> MySQL configured"
    fi
}

initMySQL() {
    if [[ -n "${APPHOST// }" ]]; then
        printf "%s\n" "-> Initializing MySQL database..."

        # check that db is reachable
        while ! nc -z "${MYSQL_HOST}" "${MYSQL_PORT}"; do
            printf "Waiting for ${MYSQL_HOST} to be reachable\n"
            sleep 2
        done

        #
        # Create app db
        #
        rake db:create
        if [[ ! -f "${SOURCE_DIR}/db/schema.db" ]]; then
            rake db:migrate
        else
            rake db:schema:load
        fi
        rake db:seed

        ##
        ## >RAILS_ENV=test rake warehouse:db:create
        ##
        printf "%s\n\n" "-> MySQL database initialized"
    fi
}

configPostgres() {
    if [[ ! -f "${SOURCE_DIR}/config/database.yml" ]]; then
        printf "%s\n-> " "-> Configuring Postgres..."

cat <<- 'EOF' > "${SOURCE_DIR}/config/database.yml"
default: &default
  adapter: postgresql
  encoding: unicode
  pool: 5
  username: <%= ENV['POSTGRES_USER'] %>
  password: <%= ENV['POSTGRES_PASSWORD'] %>
  host: <%= ENV['POSTGRES_HOST'] %>
  port: <%= ENV['POSTGRES_PORT'] %>

development:
  <<: *default
  database: <%= ENV['APPNAME'] %>_development

test:
  <<: *default
  database: <%= ENV['APPNAME'] %>_test
EOF
        # set perms
        chmod 0644 "${SOURCE_DIR}/config/database.yml"
        chown "${BUILD_USER}":"${BUILD_USER}" "${SOURCE_DIR}/config/database.yml"

        printf "%s\n" "-> Postgres configured"
    fi
}

initPostgres() {
    if [[ -n "${APPHOST// }" ]]; then
        printf "%s\n" "-> Initializing Postgres database..."

        # check that db is reachable
        while ! nc -z "${POSTGRES_HOST}" "${POSTGRES_PORT}"; do
            printf "Waiting for ${POSTGRES_HOST} to be reachable\n"
            sleep 2
        done

        #
        # Create app db
        #
        rake db:create
        if [[ ! -f "${SOURCE_DIR}/db/schema.db" ]]; then
            rake db:migrate
        else
            rake db:schema:load
        fi
        rake db:seed

        ##
        ## >RAILS_ENV=test rake warehouse:db:create
        ##
        printf "%s\n\n" "-> Postgres database initialized"
    fi
}

configSendgrid() {
    if [[ ! -f "${SOURCE_DIR}/config/sendgrid.yml" && -f "${SOURCE_DIR}/config/sendgrid.yml.example" ]]; then
        printf "%s\n-> " "-> Configuring Sendgrid..."

        cp "${SOURCE_DIR}/config/sendgrid.yml.example" "${SOURCE_DIR}/config/sendgrid.yml"
        chmod 0644 "${SOURCE_DIR}/config/sendgrid.yml"
        chown "${BUILD_USER}":"${BUILD_USER}" "${SOURCE_DIR}/config/sendgrid.yml"

        printf "%s\n" "-> Sendgrid configured"
    fi
}

configVolumes() {
    #
    # Setup volume permissions
    #
    printf "%s\n-> " "-> Setting volume permissions..."
    [[ -d "${SOURCE_DIR}/log" ]] && sudo chown "${BUILD_USER}:${BUILD_USER}" "${SOURCE_DIR}/log"
    [[ -d "${SOURCE_DIR}/node_modules" ]] && sudo chown "${BUILD_USER}:${BUILD_USER}" "${SOURCE_DIR}/node_modules"
    [[ -d "${SOURCE_DIR}/tmp" ]] && sudo chown "${BUILD_USER}:${BUILD_USER}" "${SOURCE_DIR}/tmp"
    printf "%s\n" "-> Volume permissions set"
}

updateBundles() {
    #
    # Run bundler
    #
    printf "%s\n-> " "-> Running bundler..."
    bundle check || bundle install
    printf "%s\n" "-> Bundles installed"
}

updateEnvironment() {
    #
    # Update environment
    #
    if [[ -f "${SOURCE_DIR}/.envrc" ]]; then
        printf "%s\n-> " "-> Updating environment..."
        pushd "${PWD}"
        cd "${SOURCE_DIR}"
        direnv allow
        popd
        printf "%s\n-> " "-> Environment updated"
    fi
}

updateModules() {
    #
    # Update modules
    #
    printf "%s\n-> " "-> Updating modules..."
    npm install
    printf "%s\n" "-> Modules updated"
}

setup()
{
    # mount persistent gem directory and then repair rvm
    if [[ -d "${BUNDLE_PATH}" ]]; then
        mv "${BUILD_HOME}/.rvm/gems" "${BUNDLE_PATH}"
        ln -s "${BUNDLE_PATH}/gems" "${BUILD_HOME}/.rvm/gems"

        mv "${BUILD_HOME}/.rvm/gemsets" "${BUNDLE_PATH}"
        ln -s "${BUNDLE_PATH}/gemsets" "${BUILD_HOME}/.rvm/gemsets"

        rvm repair all; rvm reload
    fi

    #
    # Install bundler
    #
    printf "%s\n-> " "-> Installing bundler..."
    gem install bundler
    printf "%s\n" "-> Bundler installed"

    #
    # Install remote debug toolchain
    #
    printf "%s\n-> " "-> Installing remote debug tools..."
    gem install debase
    gem install ruby-debug-ide
    printf "%s\n\n" "-> Remote debug tools installed"

    #
    # Install rails
    #
    printf "%s\n-> " "-> Installing rails..."
    gem install rails -v "${RAILS_VERSION}"
    gem install webdrivers
    printf "%s\n" "-> Rails installed"

    if [[ -n "${APPHOST// }" ]]; then
        #
        # Add webpacker binstubs
        #
        printf "%s\n-> " "-> Adding webpacker binstubs..."
        # rails webpacker:binstubs

        printf "%s\n\n" "-> Webpacker binstubs installed"
    fi

    #
    # Cleanup
    #
    gem update --system
}

checkConfiguration() {
    #
    # Check if app is mounted in source before continuing
    #
    printf "\n%s\n" "Checking app configuration..."
    if ! [[ -f "${SOURCE_DIR}/Gemfile" || -f "${SOURCE_DIR}/package.json" ]]; then
        printf "\n%s\n" "-> App not found!"
cat <<- EOF

It looks like there is no Rails app in the working directory:

    ${SOURCE_DIR}

Create a new Rails app with:

    prompt> rails _${RAILS_VERSION}_ new ${APPNAME:-app-dev}

Be sure to update the "COMPOSE_PROJECT_NAME" variable in the ".env" file and
then restart this container via docker-compose.

Refer to the README.md file for help.

EOF
        return 1
    fi

    printf "%s\n" "-> App found. Configuring..."
    return 0
}

source ${BUILD_HOME}/.rvm/scripts/rvm

# update volume permissions
configVolumes

# always run in app source directory!
cd "${SOURCE_DIR}" 2>&1 > /dev/null

# check for previously installed bundles
if [[ ! -f "${BUILD_HOME}/.locks/.bootstrap.lock" ]]; then
    touch "${BUILD_HOME}/.locks/.bootstrap.lock"
    updateEnvironment
    configVolumes
    setup
    checkConfiguration
    (($? == 0)) && {
        configActiveStorage
        # configMySQL
        configPostgres
        # configSendgrid
        updateBundles
        updateModules
        # initMySQL
        initPostgres
    }
else
    updateEnvironment
    updateBundles
    # remove stale app server pid
    if [[ -f "${SOURCE_DIR}/tmp/pids/server.pid" ]]; then
        rm -f "${SOURCE_DIR}/tmp/pids/server.pid"
    fi
    updateModules
fi

# make sure node_modules is owned by the app user
[[ -d "${SOURCE_DIR}/node_modules" ]] && sudo chown "${BUILD_USER}:${BUILD_USER}" "${SOURCE_DIR}/node_modules"

printf "%s\n\n" "Configuration finished"

#
# Start Xvfb. Redirect output to null because it complains when starting from
# non-priveleged mode.
#
Xvfb :99 -screen 0 1280x1024x24 > /dev/null 2>&1 &

#
# Run command (usually starts the target app)
#

# run any other command passed on cli
exec "$@"
