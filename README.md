# Docker development environment for Ruby on Rails

This repo will spin up an environment that includes all of the necessary components to support development of a Ruby on
Rails application within an isolated environment. The following components are included:

- Ruby 2.5.1
- Ruby on Rails 6.0.3.4
- Node 12
- MySQL 8 (testing)
- Postgres 10
- Redis 3.2

>NOTE: The following instructions assume images have been built already either locally or are available in a Docker
container registry. If you need to build images refer to Appendix A at the end of this document.

## Database support

The following database services are spun up:

- MySQL 8 (testing)
- Postgres 10

## Seeding the databases

Importing seed data is more easily handled within the __app__ container. The seed data should be located in the
following paths:

| database | path                                        |
|:---------|:--------------------------------------------|
| postgres | dockerfiles/data_seed/data/postgres.dump    |

Do not copy seed data into the container directly. The __/app/source__ directory represents the working directory
mounted from your host; therefore, just copy the seed data into the appropriate directy on your local host.

>Files must be named according to the table above.

### Postgres

Connect to the app shell as described in [Connecting to the app shell](#connect-app-shell). Then issue the following
commands:

```bash
prompt> data-seed-postgres.sh
```

The command will handle all of the steps for you including the intialization of material views.

## Connect to the databases

The __app__ container encapsulates runtime environment variables that facilitate connection to cluster services along
with utility scripts that refer to these variables. In summary: it's easier to use the CLI from within the __app__
container to connect to services.

>These utilty scripts have all been added to the __app__ user's path and will auto-complete at the shell prompt.

### Postgres

Within the __app__ container, connect to Postgres:

```bash
prompt> postgres-connect.sh
app@5da91485f944:~$ postgres-connect.sh
psql (10.6 (Ubuntu 10.6-1.pgdg16.04+1), server 10.1)
Type "help" for help.

app_pg_user=#
```

```bash
prompt> PGPASSWORD=app_pg_pass psql -U app_pg_user -h localhost
psql (10.6, server 10.1)
Type "help" for help.

app_pg_user=#
```

## Starting, restarting, stopping

The environment consists of multiple containers and to launch all of them, in the correct order, you will use the
`docker-compose` command.

### Starting

From the top-level project directory issue the following command to start the environment:

```bash
prompt> docker-compose -f dockerfiles/docker-compose.yml up
```

When starting the environment the first time, or any time after removing its persistent volumes, a bootstrapping stage
will be triggered that accomplishes tasks such as the following:

- setting permissions on persistent volumes, creating symlinks as needed
- installation of additional tooling (e.g. toolchains)

Most of these tasks will be skipped entirely the next time you restart the environment provided that you have not
manually deleted associated persistent volumes or deleted lock files.

### Stopping

From the top-level project directory issue the following command to stop the environment:

```bash
prompt> docker-compose -f dockerfiles/docker-compose.yml down
```

It is completely safe to stop the environment if you need to switch to a different project, reboot, etc., __provided
that you have not saved files or made changed inside of the container in a part of the filesystem that is ephemeral__.

In general, if you need to save work inside of the container, make sure you save it inside of `/app/source` which is
your local working directory mounted inside the container and therefore persistent by fact.

### Restarting

You may find you need to restart the primary app container. In particular, the remote debug server has a tendency to
halt the container.

```bash
prompt> docker-compose -f dockerfiles/docker-compose.yml restart <SERVICE_NAME>.
```

The above command will only restart the app and service containers. All cached files will be reloaded so the process
should be super fast.

<a name="connect-app-shell"></a>

## Connecting to the `app` shell

```bash
prompt> docker exec -it -e TERM=$TERM -e COLUMNS=$COLUMNS -e ROWS=$ROWS app-dev /bin/bash
```

The additional environment variables (specified with the `-e` option) are often needed to properly configure the target
terminal based upon your current local configuration. You may be able to get way with skipping these extra parameters
entirely but they are presented here for completeness.

### Add the docker-connect function locally

You can simplify the process of attaching to named docker containers by adding the following function to your `.bashrc`
file:

```bash
docker-connect() {
  local container="${1:-app-dev}"
  docker exec -it -e TERM="${TERM}" -e COLUMNS="${COLUMNS}" -e ROWS="${ROWS}" "${container}" /bin/bash
}
```

Be sure to reload your `.bashrc` afterwards:

```bash
prompt> source $HOME/.bashrc
```

Then connect to the __app__ container as follows:

```bash
prompt> docker-connect APPNAME
app@5da91485f944:~$
```

>NOTE: The `APPNAME` is set via the `COMPOSE_PROJECT_NAME` variable set in the top-level `.env` file.

## Clean up docker cluster volumes

A number of volumes will be created (within the docker host) when the docker cluster is launched. The volumes exist to
preserve data between restarts. You can clean up all associated volumes by running the following script:

```bash
prompt> dockerfiles/docker-volume-prune.sh
...
```

You will have to re-seed the databases after running this command the next time you launch the cluster.

---
markeissler.org
